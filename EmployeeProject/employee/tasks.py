from rest_framework.authtoken.admin import User
from django.core.mail import send_mail
from EmployeeProject.celery import app


@app.task
def send_reg_mail(contact_email):
    send_mail(
        "Verification",
        "Thanks for registration",
        "slavadan909@gmail.com",
        [contact_email],
        fail_silently=False
    )


@app.task
def clear_users():
    users = User.objects.filter(is_active=False)
    users.delete()
