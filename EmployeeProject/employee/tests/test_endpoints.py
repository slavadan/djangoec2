import pytest


@pytest.mark.django_db
def test_endpoint1(api_client):
    response = api_client().get(path='/api/employee/list/')
    assert response.status_code == 200
