import pytest
from employee.models import Bank


# Create your tests here.
@pytest.mark.django_db
def test_create_bank1():
    tinkoff: Bank = Bank.objects.create(
        name="tinkoff",
        email="tinkoff@gmail.com",
        web_site="http://tinkoff.ru"
    )
    assert tinkoff.name == "tinkoff"


@pytest.mark.django_db
def test_create_bank2():
    assert Bank.objects.count() == 0
    Bank.objects.create(
        name="testbank1",
        email="testbank1@gmail.com",
        web_site="http://testbank1.ru"
    )
    Bank.objects.create(
        name="testbank2",
        email="testbank2@gmail.com",
        web_site="http://testbank2.ru"
    )
    assert Bank.objects.count() == 2