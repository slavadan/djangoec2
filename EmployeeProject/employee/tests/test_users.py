import pytest


def test_create_user1(user_first):
    user_first.set_password("newpass")
    assert user_first.check_password("newpass") is True


def test_admin_user(user_admin):
    assert user_admin.is_superuser is True
    print(user_admin.first_name)
    assert user_admin.username == "Admin1"
