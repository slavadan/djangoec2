import pytest
from django.contrib.auth.models import User
from employee.models import Employee
from rest_framework.test import APIClient


@pytest.fixture()
def user_first(db):
    user = User.objects.create_user("myuser1", "myuser1@gmail.com", "myuser1")
    return user


@pytest.fixture()
def users_factory(db):
    def create_user(
            username: str,
            password: str = None,
            first_name: str = 'firstname',
            last_name: str = 'lastname',
            email: str = 'user@user.com',
            is_staff: bool = False,
            is_superuser: bool = False,
            is_active: bool = True,
    ):
        user = User.objects.create_user(
            username=username,
            password=password,
            first_name=first_name,
            last_name=last_name,
            email=email,
            is_staff=is_staff,
            is_superuser=is_superuser,
            is_active=is_active,
        )
        return user
    return create_user


@pytest.fixture()
def user_admin(db, users_factory):
    return users_factory(
        "Admin1",
        "admin",
        "Slava",
        is_superuser=True,
        is_staff=True
    )


@pytest.fixture()
def employee1(db):
    return Employee.objects.create(
        name="TestEmpl1",
        surname="TestEmpl1",
        job_position="TestEmpl1",
        is_manager=True,
        is_admin=True,
        company=None,
        personal_data=None,
    )


@pytest.fixture()
def api_client():
    return APIClient
