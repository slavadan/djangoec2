from rest_framework.decorators import api_view
from rest_framework.views import APIView
from django.db.models import Q
from django.db.models import F
from rest_framework.viewsets import ModelViewSet
from rest_framework import generics, permissions
from django.http import HttpResponse
from .serializers import (
    BankSerializer,
    CompanySerializer,
    PersonalDataSerializer,
    EmployeeSerializer
)
from .models import (
    Employee,
    Company,
    PersonalData,
    Bank
)
from rest_framework.response import Response
from rest_framework import status
from .tasks import send_reg_mail


class WatchPermission(permissions.BasePermission):

    def has_permission(self, request, view):
        if request.user.is_staff:
            return True
        return False


def is_superuser_decorator(func):
    def wrapper(request, *args, **kwargs):
        if request.user.is_superuser:
            return func(request, *args, **kwargs)

        return Response(status=status.HTTP_403_FORBIDDEN)

    return wrapper


# Create your views here.
class ListEmployeeView(APIView):

    def get_queryset(self):
        return Employee.objects.all()

    def get(self, request, format=None):
        employee = self.get_queryset()
        serializer = EmployeeSerializer(employee, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = EmployeeSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DetailEmployeeView(APIView):

    def get(self, request, pk: int, format=None):
        employee = Employee.objects.get(pk=pk)
        return Response(EmployeeSerializer(employee).data)

    def put(self, request, pk, format=None):
        employee = Employee.objects.get(pk=pk)
        serializer = EmployeeSerializer(employee, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_202_ACCEPTED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        employee = Employee.objects.get(pk=pk)
        employee.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


# block 4.12 task 1
class CompanyByPeriodView(APIView):

    def get(self, request, first_date, second_date, format=None):
        company = Company.objects.filter(
            Q(created__range=(first_date, second_date)) &
            Q(updated__lt=second_date)
        )
        return Response(CompanySerializer(company, many=True).data)


# block 4.12 task 2
class UpdateSalaryView(APIView):

    def post(self, request, number, date, format=None):
        PersonalData.objects.filter(
            date_of_birth=date).update(salary=F('salary') + number)

        return Response(
            PersonalDataSerializer(
                PersonalData.objects.filter(date_of_birth=date),
                many=True
            ).data
        )


# block 4.12 task 3
@api_view(['POST'])
def create_company_list(request):
    serializer = CompanySerializer(data=request.data, many=True)

    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)


# block 4.12 task 4
@api_view(['GET'])
def last_employers(request):
    res = []
    companies = Company.objects.all()

    for company in companies:

        employers = company.employers.all()

        if len(employers) != 0:
            res.append(
                (
                    str(employers.last().created),
                    f'company={company.name}'
                )
            )
    return HttpResponse(res)


class CompanyCreateView(generics.CreateAPIView):
    queryset = Company.objects.all()
    serializer_class = CompanySerializer

    def get(self, request, format=None):
        company = self.get_queryset()
        serializer = CompanySerializer(company, many=True)
        return Response(serializer.data)


class CompanyListView(generics.ListAPIView):
    queryset = Company.objects.all()
    serializer_class = CompanySerializer
    permission_classes = [permissions.AllowAny]

    def post(self, request, format=None, **kwargs):
        send_reg_mail.delay('swgrevenger@gmail.com')
        print('AAAAAAAAAAAAAAAAAAAAAAAAAAAA')
        return Response(status.HTTP_400_BAD_REQUEST)


class CompanyUpdateView(generics.UpdateAPIView):
    queryset = Company.objects.all()
    serializer_class = CompanySerializer

    def get(self, request, pk, format=None):
        company = Company.objects.get(pk=pk)
        serializer = CompanySerializer(company)
        return Response(serializer.data)


class CompanyModelViewSet(ModelViewSet):
    queryset = Company.objects.all()
    serializer_class = CompanySerializer
    permission_classes = [permissions.AllowAny]


@api_view(['GET', 'POST'])
@is_superuser_decorator
def bank_list_view(request):
    queryset = Bank.objects.all()

    if request.method == 'GET':
        return Response(BankSerializer(queryset, many=True).data)

    if request.method == 'POST':
        serializer = BankSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)


@api_view(['GET', 'PUT', 'DELETE'])
def bank_detail_view(request, pk):
    bank = Bank.objects.get(pk=pk)

    if request.method == 'GET':
        return Response(BankSerializer(bank).data)

    if request.method == 'PUT':
        serializer = BankSerializer(bank, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_202_ACCEPTED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    if request.method == 'DELETE':
        bank.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


# block 4.10 task 1
class PersonalDataViewSet(ModelViewSet):
    queryset = PersonalData.objects.all()
    serializer_class = PersonalDataSerializer
    permission_classes = [permissions.IsAuthenticated]
