from rest_framework import serializers
from .models import (
    DateModel,
    Employee,
    Company,
    Bank,
    PersonalData
)


class DateSerializer(serializers.ModelSerializer):

    class Meta:
        model = DateModel
        fields = '__all__'


class BankSerializer(serializers.ModelSerializer):

    class Meta:
        model = Bank
        fields = '__all__'


class CompanySerializer(serializers.ModelSerializer):

    class Meta:
        model = Company
        fields = [
            'name',
            'web_site',
            'email',
            'post_index',
            'logo',
            'banks',
            'employers',
        ]


class PersonalDataSerializer(serializers.ModelSerializer):

    class Meta:
        model = PersonalData
        fields = '__all__'


class EmployeeSerializer(serializers.ModelSerializer):

    class Meta:
        model = Employee
        fields = '__all__'

    def to_representation(self, instance):
        self.fields['company'] = CompanySerializer(read_only=True)
        self.fields['personal_data'] = PersonalDataSerializer(read_only=True)
        return super(EmployeeSerializer, self).to_representation(instance)
