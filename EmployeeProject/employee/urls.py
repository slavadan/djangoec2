from django.urls import path, include
from .views import (
    ListEmployeeView,
    DetailEmployeeView,
    CompanyByPeriodView,
    UpdateSalaryView,
    create_company_list,
    last_employers,
    CompanyCreateView,
    CompanyListView,
    CompanyUpdateView,
    CompanyModelViewSet,
    bank_list_view,
    bank_detail_view,
    PersonalDataViewSet,
)
from rest_framework.routers import DefaultRouter


router = DefaultRouter()
router.register(r'personalset', PersonalDataViewSet, basename='Data')
router.register(r'modelset', CompanyModelViewSet, basename='set')

app_name = 'employee'

urlpatterns = [
    path('api/employee/list/', ListEmployeeView.as_view()),
    path('api/employee/<int:pk>/', DetailEmployeeView.as_view()),

    path('api/company/create/', CompanyCreateView.as_view()),
    path('api/company/list/', CompanyListView.as_view()),
    path('api/company/create/list', create_company_list),
    path('api/company/update/<int:pk>/', CompanyUpdateView.as_view()),
    path(
        'api/company/<str:first_date>/<str:second_date>/',
        CompanyByPeriodView.as_view()
    ),
    path('api/lastemployers/', last_employers),

    path('api/bank/list/', bank_list_view),
    path('api/bank/<int:pk>/', bank_detail_view),

    path('data/', include(router.urls)),
    path('update/data/<int:number>/<str:date>', UpdateSalaryView.as_view()),
]

urlpatterns += router.urls
