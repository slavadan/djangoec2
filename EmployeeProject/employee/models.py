from django.contrib.auth.models import AbstractUser, UserManager, User
from django.db import models


# Create your models here.
class DateModel(models.Model):
    created: models.DateField = models.DateField(auto_now_add=True)
    updated: models.DateField = models.DateField(auto_now=True)

    class Meta:
        abstract = True


class Employee(DateModel):
    name: models.CharField = models.CharField(max_length=255)
    surname: models.CharField = models.CharField(max_length=255)
    job_position: models.CharField = models.CharField(max_length=255)
    is_manager: models.BooleanField = models.BooleanField()
    is_admin: models.BooleanField = models.BooleanField()
    company: models.ForeignKey = models.ForeignKey(
        'Company',
        on_delete=models.CASCADE,
        related_name="employers",
        null=True
    )
    personal_data: models.OneToOneField = models.OneToOneField(
        'PersonalData',
        on_delete=models.CASCADE,
        related_name="empls_personal_data",
        null=True,
        blank=True
    )

    def __str__(self):
        return self.name


class Company(DateModel):
    name: models.CharField = models.CharField(max_length=255)
    web_site: models.URLField = models.URLField()
    email: models.EmailField = models.EmailField()
    post_index: models.CharField = models.CharField(max_length=10)
    logo: models.CharField = models.CharField(max_length=10)
    banks: models.ManyToManyField = models.ManyToManyField("Bank")

    def __str__(self):
        return self.name


class Bank(DateModel):
    name: models.CharField = models.CharField(max_length=255)
    web_site: models.URLField = models.URLField()
    email: models.EmailField = models.EmailField()


class PersonalData(DateModel):
    employee: models.OneToOneField = models.OneToOneField(
        'Employee',
        on_delete=models.CASCADE,
        related_name="personal_data_of_empl",
        primary_key=True
    )
    date_of_birth: models.DateField = models.DateField()
    home_address: models.CharField = models.CharField(max_length=90)
    salary: models.PositiveIntegerField = models.PositiveIntegerField()


class Training(models.Model):
    name = models.CharField(max_length=100)
    picture = models.FileField(upload_to='media/')
