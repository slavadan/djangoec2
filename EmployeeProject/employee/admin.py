from django.contrib import admin
from .models import Company, Employee, Bank, PersonalData, Training

# Register your models here.
admin.site.register(Employee)
admin.site.register(Company)
admin.site.register(Bank)
admin.site.register(PersonalData)
admin.site.register(Training)
