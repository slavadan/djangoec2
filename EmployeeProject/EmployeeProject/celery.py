import os
from celery import Celery
from celery.schedules import crontab

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'EmployeeProject.settings')

app = Celery('EmployeeProject')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()

app.conf.beat_schedule = {
    'clear-database-task':
        {
            'task': 'employee.tasks.clear_users',
            'schedule': crontab(minute='*/1'),
        }
}
